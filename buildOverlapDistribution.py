#Copyright (c) 2014, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.
import sys,os
import arcpy
import csv
import datetime
from operator import add
from itertools import groupby
from itertools import chain
from operator import itemgetter
from collections import OrderedDict
from collections import defaultdict
import operator
import takefunctions


def evenElement(a):
    return a[1::2]

def oddElement(a):
    return a[::2]

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def computeAlert(projectFID,takeDict,prjManageEagleCnt,overlapTake,onepct,fivepct):
    alert = []
    for fid in projectFID:
        tmp = prjManageEagleCnt[fid]
        
        pt = evenElement(list(chain(*tmp)))
        tmp = overlapTake[fid]
        if tmp != 0:
            olt = evenElement(list(chain(*tmp)))
        else:
            olt = 0
        tmp = onepct[fid]        
        opt = evenElement(list(chain(*tmp)))
        tmp = fivepct[fid]        
        fpt = evenElement(list(chain(*tmp)))
        projectName = ""
        
        if olt != 0:
            finalTake = [x+y for x,y in zip(pt,olt)]
        else:
            finalTake = pt
            
        for key in takeDict.iterkeys():
            if key == fid:
                prj = takeDict[key][1]
            
        alert.append([prj,"Red",sum(finalTake),sum(fpt)])
        alert.append([prj,"Yellow",sum(finalTake),sum(opt)])
    
    return alert
def buildTransformList(keyList,masterList,index):
    tempList = []
    targetList = []
    for row in keyList:
        for item in masterList:
            if row == item[0]:
                tempList.append(item[index])
        targetList.append([row,tempList])
        del tempList
        tempList=[]
    tl = tuple(targetList)
    
    finalTargetList = []
    for elem in targetList:
        thisList = list(OrderedDict.fromkeys(elem[1]))
        finalTargetList.append((elem[0],thisList))
   
    return list(finalTargetList)

def buildOverlapDistribution(managementLayer,focalProjectFID,targetFIDField,joinFIDField,mileSQField,manUnitID,manageEagleLayer,manAreaField,
                             manCountField,prjFinal,intersectedTake,lat,lng,spitsTakePoints,authorized,externalTake,eagleType,lowerThreshold,
                             upperThreshold,summary):
    # get bcr areas and number
    manageCursor = arcpy.SearchCursor(managementLayer)
    manageRow = manageCursor.next()
    manageDict = {}
    eagleCount = {}
    
    # get management unit ID and eagle count per unit
    while manageRow:
        area = manageRow.getValue(manAreaField)
        manID = manageRow.getValue(manUnitID)
         
        count = manageRow.getValue(manCountField)
        if not manageDict.has_key(manID):
            manageDict[manID] = area 
            eagleCount[manID] = count
        manageRow = manageCursor.next()
    del manageRow,manageCursor
    
    densityDict = dict()
    
    # calculate eagle density per management unit and store in a dictionary
    for eagleKey in eagleCount.iterkeys():
        for manageKey in manageDict.iterkeys():
            if eagleKey == manageKey:
                if not densityDict.has_key(manageKey):
                    count = float(eagleCount[manageKey])
                    tmp =(str(manageDict[manageKey]))
                    manAreaField = float(tmp)
                    densityDict[eagleKey] = count/manAreaField
                  
    fidList = []
    
    cursor = arcpy.UpdateCursor(managementLayer)
    row = cursor.next()
    
    # get all of the individual polygons in the merged management poly 
    
    while row:
        targetFID = row.getValue(targetFIDField)
        joinFID = row.getValue(joinFIDField)    
        milesSQ = row.getValue(mileSQField)
        manID = row.getValue(manUnitID)
        row = cursor.next()
        fidList.append((targetFID,joinFID,milesSQ,(manID)))
        
    fidList = set(fidList)    
    newFidList = sorted(fidList,key=itemgetter(0,1,3))
    
  
    
    del row,cursor
    
    polyFID=[]
    
    for key,items in groupby(newFidList,key=itemgetter(0)):
        polyFID.append(key)
    
    # find all polygons contributing to intersection 
    
    polyParticipateList=buildTransformList(polyFID,newFidList,1)
    
    # get participating management units in each major polygon slice
    manageList=buildTransformList(polyFID,newFidList,3)
    newManageList=[]
    
    for row in manageList:
        munit = row[1]
        munit = sorted(munit)
        newManageList.append(munit)    

    srcPlyArea = []
    srcPlyArea=buildTransformList(polyFID,newFidList,2)
    areaValues = []
    

    sourcePolyArea = dict()
    # sum management area values for each major piece of poly
    for index in range(0,len(srcPlyArea)):
        areaValues.append(srcPlyArea[index][1])
        sourcePolyList = srcPlyArea[index][1]
        temp = reduce(lambda x,y:x+y,sourcePolyList)
        sourcePolyArea[polyFID[index]] = temp
    
    manAreaField=[]
    
    # associate a management zone with area 
    for management,area in zip(newManageList,areaValues):
        newList = zip(management,area)
        manAreaField.append(newList)
       
    polyArea = []
    managementDict = OrderedDict()
    
    for idx in range(0,len(polyFID)):
        managementDict[idx] = manAreaField[idx]
    
    finalList={}
    
    for row in range(0,len(polyFID)):
        finalList[row] = [[polyParticipateList[row][1],managementDict[row]]]    
    
    # grab the points that have been intersected with the managementLayer shapefile. capture lat long to determine points outside of project laps
    if (len(intersectedTake) > 1):    

        cursor = arcpy.UpdateCursor(intersectedTake)
        row = cursor.next()
        intTake = []
        while row:
            _lat  = row.getValue(lat)
            _long  = row.getValue(lng)
            fid = row.getValue(joinFIDField)
            intTake.append([fid,_lat,_long])
            row = cursor.next()
        del cursor,row
        
        intersectedTakeSet = set([tuple(sorted(x)) for x in intTake])
          
        # get all spits take points 
        
        cursor = arcpy.UpdateCursor(spitsTakePoints)
        row = cursor.next()
        primaryTakePoints = []
        
        while row:
                 
            _lat = row.getValue(lat)
            
            _long = row.getValue(lng)
            
            auth = row.getValue(authorized)
            primaryTakePoints.append([_lat,_long,auth])
            row = cursor.next()
        del cursor,row
        
        authTake = []
        # authorized take appends fid and project take values (there may be multiple fids as well) - capture lat long as well
        try:
            for item in primaryTakePoints:
                for item2 in intersectedTakeSet:
                    if item[0] == item2[2] and item[1] == item2[0]:
                        authTake.append((item2[1],item[2],item[0],item[1]))
        except e:
            pass
    # compute peripheral take (take that occurs outside of project buffers)
    peripheralTake={}
    
    if (len(externalTake) > 1):
        cursor = arcpy.SearchCursor(externalTake)
        row = cursor.next()
        peripheralTakePoints = []
        
        while row:
            man  = row.getValue(manUnitID)
            auth = row.getValue(authorized)
            peripheralTakePoints.append([(man),int(auth)])
            row = cursor.next()
        del cursor,row
        
        # sum authorized take per management unit
        
    
        for item in peripheralTakePoints:
            if item[0] in peripheralTake.iterkeys():
                peripheralTake[item[0]] += item[1]
            else:
                peripheralTake[item[0]] = item[1]
        
    # get project IDs from polyParticipateList
    polyPart = [y for x,y in polyParticipateList]
    projectFID = set(sum(polyPart,[]))
     
    takeAreaDict={}
    
    pt = OrderedDict()
    
    for FID in projectFID:
        _overlapTake,prjArea,takeDict,prjTake,sumLapArea = takefunctions.computeProjectTake(FID,projectFID,prjFinal,focalProjectFID,densityDict,polyParticipateList,finalList,eagleType)
        try:
            val = prjTake[0][0]
            pt[val] = prjTake[0][1]
        except:
            val = 0
            pt[val] = 0
        
        area = prjArea[FID]
        
        try:
            ovlTake = _overlapTake[0][1]
        except:
            ovlTake = OrderedDict([(FID,0)])
            
        thislist = {}
        
        ot = [x for x,y in ovlTake.iteritems()]
        ar = [x for x,y in area.iteritems()]
      
        diff = set(ot) - set(ar)
        if diff:
            for item in diff:
                area[item] = 0
        for key in ovlTake:
            thislist[key]=(ovlTake[key],area[key])
        takeAreaDict[FID] = thislist
    projectTake = OrderedDict()    
    try:
        projectTake = OrderedDict(sorted(pt.iteritems(), key=lambda x: len(x[1]),reverse=True))
    except:
        for fid in projectFID:
            projectTake[fid]={(fid,0)}
    
    tmpList = []
    for FID in projectFID:    
        for item in finalList:
            x = finalList[item]
            if FID in x[0][0]:
                tmpList.append([FID,x[0][1]])
    
    groups = groupby(tmpList,key=operator.itemgetter(0))
    prjManList = [{'fid':key, 'manarea':[x[1] for x in items]} for key, items in groups]
     
    managementSummaryDict={}
    
    # sum areas by management unit id within LAP
    for items in prjManList:
        for i in items:
            if i != 'fid':
                tmplist = list(chain(*items[i]))
                tmplist = sorted(tmplist)
               
                managementUnit = defaultdict(list)
                for key, area in tmplist:
                    managementUnit[key].append(area)       
                
                managementSummary = []
     
                for k,v in managementUnit.iteritems():
                    managementSummary.append([k,sum(v)])
            else:
                fid = items[i]
        managementSummaryDict[fid] = managementSummary            
            
    olt = OrderedDict()
    for fid in projectFID:
        overlapTakeValue = takeAreaDict[fid]
        prjTake = projectTake[fid]
         
        lapAreaList = managementSummaryDict[fid]
        newlist=[]
        for laparea in lapAreaList:
            v = laparea[0]
            newlist.append([laparea[0],0])
           
            for x,prjtake in prjTake:
                if v == x:
                    if v in overlapTakeValue:
                        ovrlptk = overlapTakeValue[v][0]
                        adjtake = float(ovrlptk) 
                        newlistlen = len(newlist)
                        newlist[newlistlen-1][1] = adjtake
                           
        olt[fid] = newlist
    
    overlapTake = OrderedDict(sorted(olt.iteritems(), key=lambda x: len(x[1]),reverse=True))
    
    prjManageEagleCnt = {}
    
    tmpList2 = []
    
    # compute eagle density for each management unit within LAP
    
    for fid in projectFID:
        locallist = managementSummaryDict[fid]    
        for item in locallist:    
            for key in densityDict:
                if item[0] == (key):
                    tmpList2.append([item[0],densityDict[key] * item[1]])

        prjManageEagleCnt[fid] = tmpList2
        tmpList2 = []
    
        
    lowerPct = {}
    upperPct = {}
    
    lwrThres = float(lowerThreshold)/100
    uprThres = float(upperThreshold)/100
    for fid in projectFID:
        locallist = prjManageEagleCnt[fid]
        chainedList = evenElement(list(chain(*locallist)))
        manageList = oddElement(list(chain(*locallist)))
        newLocalList = map(lambda x:x*lwrThres,chainedList)
        lowerPct[fid] = zip(manageList,newLocalList)
        newLocalList = map(lambda x:x*uprThres,chainedList)
        upperPct[fid] = zip(manageList,newLocalList)
    
    alert = computeAlert(projectFID,takeDict,prjManageEagleCnt,overlapTake,lowerPct,upperPct)
     
    takefunctions.takeSummary(summary,focalProjectFID,takeDict,projectFID,projectTake,overlapTake,prjManageEagleCnt,lowerThreshold,upperThreshold,alert)
    
    fh = open(summary,"a")
    fh.close()
  
    
def buildSingleProjectDistribution(managementLayer,focalProjectFID,targetFIDField,joinFIDField,mileSQField,manUnitID,manageEagleLayer,manAreaField,
                             manCountField,prjFinal,intersectedTake,lat,lng,spitsTakePoints,authorized,externalTake,eagleType,lowerThreshold,
                             upperThreshold,summary):
    manageCursor = arcpy.SearchCursor(managementLayer)
    manageRow = manageCursor.next()
    manageDict = {}
    eagleCount = {}
    
    # get management unit ID and eagle count per unit
    while manageRow:
        area = manageRow.getValue(manAreaField)
        manID = manageRow.getValue(manUnitID)
         
        count = manageRow.getValue(manCountField)
        if not manageDict.has_key(manID):
            manageDict[manID] = area 
            eagleCount[manID] = count
        manageRow = manageCursor.next()
    del manageRow,manageCursor
    
    densityDict = dict()
    
    # calculate eagle density per management unit and store in a dictionary
    for eagleKey in eagleCount.iterkeys():
        for manageKey in manageDict.iterkeys():
            if eagleKey == manageKey:
                if not densityDict.has_key(manageKey):
                    count = float(eagleCount[manageKey])
                    tmp =(str(manageDict[manageKey]))
                    manAreaField = float(tmp)
                    densityDict[eagleKey] = count/manAreaField
                  
    fidList = []
    
    cursor = arcpy.UpdateCursor(managementLayer)
    row = cursor.next()
    
    # get all of the individual polygons in the merged management poly 
    
    while row:
        targetFID = row.getValue(targetFIDField)
        joinFID = row.getValue(joinFIDField)    
        milesSQ = row.getValue(mileSQField)
        manID = row.getValue(manUnitID)
        row = cursor.next()
        fidList.append((targetFID,joinFID,milesSQ,(manID)))
        
    fidList = set(fidList)    
    newFidList = sorted(fidList,key=itemgetter(0,1,3))
    
  
    
    del row,cursor
    
    polyFID=[]
    
    for key,items in groupby(newFidList,key=itemgetter(0)):
        polyFID.append(key)
    
    # find all polygons contributing to intersection 
    
    polyParticipateList=buildTransformList(polyFID,newFidList,1)
    
    # get participating management units in each major polygon slice
    manageList=buildTransformList(polyFID,newFidList,3)
    newManageList=[]
    
    for row in manageList:
        munit = row[1]
        munit = sorted(munit)
        newManageList.append(munit)    
    projectTake = {} 
    templist = []
    for item in newManageList:
        for item2 in item:
            templist.append((item2,0))
    projectTake[focalProjectFID] = templist
    srcPlyArea = []
    srcPlyArea=buildTransformList(polyFID,newFidList,2)
    areaValues = []
    
    sourcePolyArea = dict()
    # sum management area values for each major piece of poly
    for index in range(0,len(srcPlyArea)):
        areaValues.append(srcPlyArea[index][1])
        sourcePolyList = srcPlyArea[index][1]
        temp = reduce(lambda x,y:x+y,sourcePolyList)
        sourcePolyArea[polyFID[index]] = temp
    
    manAreaField=[]
    
    # associate a management zone with area 
    for management,area in zip(newManageList,areaValues):
        newList = zip(management,area)
        manAreaField.append(newList)
        
    polyArea = []
    managementDict = OrderedDict()
    
    for idx in range(0,len(polyFID)):
        managementDict[idx] = manAreaField[idx]
    
    finalList={}
    
    for row in range(0,len(polyFID)):
        finalList[row] = [[polyParticipateList[row][1],managementDict[row]]]    
    
    # grab the points that have been intersected with the managementLayer shapefile. capture lat long to determine points outside of project laps
    
    
    # get project IDs from polyParticipateList
    polyPart = [y for x,y in polyParticipateList]
    projectFID = set(sum(polyPart,[]))
     
    takeAreaDict={}
      
    
    for FID in projectFID:
        _overlapTake,prjArea,takeDict,prjTake,sumLapArea = takefunctions.computeProjectTake(FID,projectFID,prjFinal,focalProjectFID,densityDict,polyParticipateList,finalList,eagleType)
        area = sumLapArea
    
    tmpList = []
    for FID in projectFID:    
        for item in finalList:
            x = finalList[item]
            if FID in x[0][0]:
                tmpList.append([FID,x[0][1]])
    
    groups = groupby(tmpList,key=operator.itemgetter(0))
    prjManList = [{'fid':key, 'manarea':[x[1] for x in items]} for key, items in groups]
     
    managementSummaryDict={}
    
    # sum areas by management unit id within LAP
    for items in prjManList:
        for i in items:
            if i != 'fid':
                tmplist = list(chain(*items[i]))
                tmplist = sorted(tmplist)
               
                managementUnit = defaultdict(list)
                for key, area in tmplist:
                    managementUnit[key].append(area)       
                
                managementSummary = []
     
                for k,v in managementUnit.iteritems():
                    managementSummary.append([k,sum(v)])
            else:
                fid = items[i]
        managementSummaryDict[fid] = managementSummary            
            
    
    # compute eagle density for each management unit within LAP
    prjManageEagleCnt = {}
    tmplist = []
    for fid in projectFID:
        locallist = managementSummaryDict[fid]    
        for item in locallist:    
            for key in densityDict:
                if item[0] == key:
                    tmplist.append([item[0],densityDict[key] * item[1]])
    
        prjManageEagleCnt[fid] = tmplist
        
    lowerPct = {}
    upperPct = {}
    
    lwrThres = float(lowerThreshold)/100
    uprThres = float(upperThreshold)/100
    for fid in projectFID:
        locallist = prjManageEagleCnt[fid]
        chainedList = evenElement(list(chain(*locallist)))
        manageList = oddElement(list(chain(*locallist)))
        newLocalList = map(lambda x:x*lwrThres,chainedList)
        lowerPct[fid] = zip(manageList,newLocalList)
        newLocalList = map(lambda x:x*uprThres,chainedList)
        upperPct[fid] = zip(manageList,newLocalList)
    overlapTake = {}
    overlapTake[FID] = 0
    alert = computeAlert(projectFID,takeDict,prjManageEagleCnt,overlapTake,lowerPct,upperPct)
    
    takefunctions.takeSummary(summary,focalProjectFID,takeDict,projectFID,projectTake,overlapTake,prjManageEagleCnt,lowerThreshold,upperThreshold,alert)
    
    fh = open(summary,"a")
    fh.close()    
