#Copyright (c) 2014, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.

import sys, os
from collections import OrderedDict
from itertools import groupby
from operator import itemgetter
from operator import add
import datetime
import arcpy

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def buildTakeCounts(pointTake,dateVal):
    unpermitTakeCount = {}
    takeDate = []
    takeTypeDate = {}
    sortedTake = sorted(pointTake,key = itemgetter(1))
    groupTake = groupby(sortedTake, key = itemgetter(1))
        
    for key,items in groupTake:
        unpermitTakeCount[key] = sum(item[2] for item in items)

    for key in unpermitTakeCount:
        for item in pointTake:
            if key == item[1]:
                takeDate.append([item[0],item[2]])
        takeTypeDate[key] = takeDate
        takeDate=[]

    deltaDate = {}    
    
    endIntervalYear = str(dateVal).split("-")[1]
    startIntervalYear = str(dateVal).split("-")[0]
    allDates = []
    
    
    earliestDate = datetime.datetime(2100,12,31)
    latestDate = datetime.datetime(1,1,1)
    
    totalCount = 0
    intervalCount = 0
  
    finalActivityTake = []
    
    for key in takeTypeDate:
        
        takeDate = takeTypeDate[key]
        
        for date in takeDate:
            quantity = date[1]
            if date[0] <= earliestDate:
                earliestDate = date[0]
            if date[0] >= latestDate:
                latestDate = date[0]
            if key == "Collision (nonspecific)":
                pass
            takeYear = date[0].year
            if takeYear >= int(startIntervalYear) and takeYear <= int(endIntervalYear):
                intervalCount += quantity
	
	finalActivityTake.append([key,unpermitTakeCount[key],earliestDate,latestDate,intervalCount])
        earliestDate = datetime.datetime(2100,12,31)
        latestDate = datetime.datetime(1,1,1)
        intervalCount = 0
        
    return finalActivityTake 


def computePointTake(takeType,takeShapefile,eagleType,dateVal,output):
    cursor = arcpy.UpdateCursor(takeShapefile)
    row = cursor.next()
    unpermitTake = []
    removeDups = set()
   
    while row:
        if takeType == "PMT":
            species = row.getValue("SPCode")
            ddate = row.getValue("EffDate")
        if takeType == "UNPMT":
            species = row.getValue("SPITSCode")   
            ddate  = row.getValue("DiscDate")
	
        if eagleType in species and ddate != None:   
	    
            if takeType == "PMT":
                suscim  = row.getValue("Act")
                quantity = row.getValue("Auth")
                permitNum = row.getValue("PermNo")
            else:
                suscim  = row.getValue("CIMDet")
		quantity = row.getValue("Quantity")
	    lat = row.getValue("Lat")
            lon = row.getValue("Long")
            localStr = str(lat) + str(lon) + str(ddate) + str(suscim) + str(quantity)
            unpermList = set(localStr)
            if unpermList not in removeDups:
                removeDups.add(localStr)
                unpermitTake.append([ddate,suscim,int(quantity)])
        row = cursor.next()
   
    del cursor,row
    unpermitTake = sorted(unpermitTake, key = itemgetter(2, 1))
    
    finalTakeLAP = buildTakeCounts(unpermitTake,dateVal)
    
    
    unpermitTake = sorted(unpermitTake, key = itemgetter(1))
    
    takemanUnit = []
    takemanUnitDict = OrderedDict()
    takemanUnitDict[0] = finalTakeLAP 
    
    fh = open(output,"w")
    if takeType == "UNPMT":
        fh.write("OTHER UNPERMITTED TAKE,,,Discovery Period")
    elif takeType == "PMT":
        fh.write("SPITS Authorized Take \n")
    fh.write("\n")
    
    if eagleType == "GOEA":
        fh.write("Golden Eagle,All Known,Reported Years," + dateVal )
    elif eagleType == "BAEA":
	fh.write("Bald Eagle,All Known,Reported Years," + dateVal )
		
    fh.write("\n")
    
    tkd = takemanUnitDict[0]
   
    if len(tkd) == 0:
	fh.write("No unpermitted take within 2x LAP radius \n")
    for item in tkd:
        firstReport = str(item[2])[0:4]
        lastReport = str(item[3])[0:4]
        fh.write(str(item[0]) + "," + str(item[1]) +"," + firstReport + "-" + lastReport + "," + str(item[4])  + "\n")
    fh.close()

 
def deleteFeatures(source,target):
    
    takeBufferCursor = arcpy.SearchCursor(source)
    
    for row1 in takeBufferCursor:
        x = row1.getValue("Long")
        y = row1.getValue("Lat")
    
        takeCopyCursor = arcpy.UpdateCursor(target)    
        for row in takeCopyCursor:
            x1 = row.getValue("Long")
            y1 = row.getValue("Lat")
            if x == x1:
                if y == y1:
                    takeCopyCursor.deleteRow(row)
        del takeCopyCursor
    
    del takeBufferCursor

def computeProjectTake(FID,projectFID,prjFinal,focalProjectFID,densityDict,polyParticipate,finalList,eagleType):
    # sum area for project LAP for a single FID
    LAPArea = {}
    manDict = {}
    for fid in projectFID:
        for key in finalList:
            rowList = finalList[key]
            tmpList = list(rowList[0][0])
            if fid in tmpList:
                newList = list(rowList[0][1])
                for item in newList:
                    key = item[0]
                    area = item[1]
                    if key in manDict:
                        manDict[key] += area
                    else:
                        manDict[key] = area
        LAPArea[fid] = manDict #item[1] for item in newList)
        manDict = {}
    FIDdict = OrderedDict()
    for item in projectFID:
            FIDdict[item]= 0
    
    # get polygons intersecting project and sum the intersected area 
    # NOTE: project with 0 area is the project with the current FID parameter value
    for key in finalList:
        rowList = finalList[key]
	tmpList = list(rowList[0][0])
        
        if len(tmpList) > 1:
            for key in FIDdict:
                if FID in tmpList and FID != key:
                    if key in tmpList:
                        manUnitList = rowList[0][1]
                        if  FIDdict[key] != 0:
                            mulist = FIDdict[key]
                            thislist = [y for x,y in manUnitList]
                        else:
                            FIDdict[key] = (manUnitList)
    pctOverlap = []
    
    # get intersected area
    sumLapArea = {}
    for key in FIDdict:
        intersect = FIDdict[key]
        area = LAPArea[key]
        total = 0
        for val in area:
            total += area[val]
        sumLapArea[key] = total
        
        
        if  FID != key and intersect !=0:
            for item in intersect:
                pctOverlap.append((key, item[0], item[1] / area[item[0]]))

    
    cursor = arcpy.UpdateCursor(prjFinal)
    
    # get take values, project name and associate these with fid
    row = cursor.next()
    takeDict = {}
    
    while row:
        take = row.getValue(eagleType + "Take")
        fid = row.getValue("FID_prjbuf")
        prjName = row.getValue("Prj")
       

        for key in FIDdict:
            if key == fid:
                if key not in takeDict:
                    takeDict[key] = [take,prjName]
        row = cursor.next()
    del row,cursor
    
    intersectTake = []
    projectTake = []
    
    for key in FIDdict:
        if key != FID:
            takeVal = takeDict[key]
            manUnit = []
            val = takeVal[0]
            for item in pctOverlap:
                if item[0] == key:
                    manUnit.append((item[1],item[2]))
         
    
            x=LAPArea[key]
            
            unitKey = LAPArea[key]
            
            perUnitTakeCount = []
            testSet = set()
            for item in unitKey:
                for item2 in manUnit:
                    if item not in testSet:
                        testSet.add(item)
			perUnitTakeCount.append((item,takeVal[0]*unitKey[item]/sumLapArea[key]))
            if len(testSet) == 0:
		for item in unitKey:
		    perUnitTakeCount.append((item,takeVal[0]*unitKey[item]/sumLapArea[key]))		

            # pad out management unit if overlap contains fewer management units than the intersecting project
            mu = [k for k,l in manUnit]
            putc =  [m for m,n in perUnitTakeCount]
            if len(mu) != len(putc):
                diff = set(putc) - set(mu)
                for item in diff:
                    manUnit.append([item,0.0])
            
                
            perUnitTakeCount = sorted(perUnitTakeCount,key = lambda putc:putc[0])
	    manUnit = sorted(manUnit,key = lambda mu:mu[0])
                
            overlapTakeFinal = [x*y for x,y in zip([y for x, y in manUnit], [i for h,i in perUnitTakeCount])]

            tmplist = zip([x for x,y in manUnit],overlapTakeFinal)
            unitDict={}
            for item in tmplist:
                unitDict[item[0]] = item[1]
            intersectTake.append((key,unitDict))
	    
            if len(perUnitTakeCount) == 0:
		perUnitTakeCount.append([0,0])
            projectTake.append([key,perUnitTakeCount])
    

    return intersectTake,LAPArea,takeDict,projectTake,sumLapArea


def takeSummary(summaryFileName,focalProjectFID,takeDict,projectFID,takeList,overlapTake,prjManageEagleCnt,lwrThreshold,uprThreshold,alert):
    summaryFileExists = False
    
	    
    try:
        with open(summaryFileName):
            summaryFileExists = True
    except IOError:
        pass
    
    try:
	overlapTakeExists = overlapTake[focalProjectFID]
    except:
	overlapTakeExists = 0
    
    fh=open(summaryFileName,"a")
    summaryList =[]
    for key in projectFID:
        summaryList.append((key,takeList[key]))
    project = []    
    buildManagementLayer = []
    
    for element in summaryList:
        prjName = element[0]

        for val in element[1]:
           
            project.append((prjName,val))
    
    totalArea = []    
    buildManagementLayer = sorted([x for x,y in prjManageEagleCnt[focalProjectFID]])
    
    transition = focalProjectFID
    currentIndex = 0
    
    if summaryFileExists == False:
	fh.write("\n")
	fh.write("POPULATION ESTIMATES\nArea,Estimated Number of Eagles\n")
	
	manageTakeValuesLAP = prjManageEagleCnt[focalProjectFID]
	
	takeval = 0
	temp = takeList[focalProjectFID]
	
	totalTake = 0
	for unit,dummy in temp:
	    
	    val = [float(y) for x,y in manageTakeValuesLAP if x == unit]
	    if len(val) == 0:
		takeval = manageTakeValuesLAP
	    else:
		takeval = float(str(val)[1:-1])
	    totalTake += takeval
	    
	    fh.write( str(takeDict[focalProjectFID][1]) + " BMU " + str(unit) +  ",%.2f" %(takeval) + "\n")
	
	fh.write( str(takeDict[focalProjectFID][1]) + " Local Area Population (total),%.2f"%(totalTake) + "\n") 
	fh.write("\n")  	
	
	alertList = []
	for value in alert:
	    alertStatus = value[1]
	    if takeDict[focalProjectFID][1] == value[0]:
		if alertStatus == "Yellow":
		    alertList.append(lwrThreshold + "% LAP Benchmark," + "%.2f"%(value[3]) + "\n")	    
		elif alertStatus == "Red":
		    alertList.append( uprThreshold + "% LAP Benchmark," + "%.2f"%(value[3]) + "\n")
        fh.write(str(alertList[1]))
	fh.write(str(alertList[0]))
	
	
	fh.write("\n")  
	header = "Established Projects with Overlapping LAPs,Estimated Annual Take,Percent Overlap Focal Project,Overlapping Take Adjustment"
	fh.write(header)
	fh.write("\n")	
	if overlapTakeExists == 0:
	    fh.write("No overlapping established projects\n")
    if overlapTakeExists != 0:
	prjTake = float(str([value[0] for key,value in takeDict.items() if key != focalProjectFID])[1:-1])
	projectName = str([value[1] for key,value in takeDict.items() if key != focalProjectFID])[1:-1]
	projectName = projectName.replace("u'","")
	  
	doNotWriteAgain = False
	for fid in projectFID:
	    
	    if fid == focalProjectFID:
		if is_number(projectName[0:1]) != True:
		    fh.write(projectName[0:-1])
		else:
		    fh.write("Project " + projectName[0:-1])
		temp = overlapTake[fid]	
		takeval = sum([val for unit,val in temp])
		try:
		    pctOverlap = takeval / prjTake * 100
		except:
		    pctOverlap = 0
		    
		fh.write(",%.2f"%(prjTake) + ",%.2f"%(pctOverlap) + "%%,%.2f"%(takeval))
		

    fh.write("\n")  
    fh.close()    
