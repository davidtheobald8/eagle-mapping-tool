#Copyright (c) 2016, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.

import sys, os

import re
import datetime
import time
import logging


import spatialfunctions
import summaryfunctions
import singlecalcfunction


import archook
try:
    import archook #The module which locates arcgis
    archook.get_arcpy()
    import arcpy
except ImportError:
    pass


arcpy.env.overwriteOutput = True 


sysArgs = sys.argv[1:]

eagleSpecies = sysArgs[0]
focalProject = sysArgs[1]
nonEstablishedProjects = sysArgs[2]
establishedProjects = sysArgs[3]
spitsLayer = sysArgs[4]
unpermittedTakeLayer = sysArgs[5]
managementLayer = sysArgs[6]
lapRadius = sysArgs[7]
lowerThreshold = sysArgs[8]
upperThreshold = sysArgs[9]
dateRange = sysArgs[10]
summaryOutput = sysArgs[11]


eagleType = ""
if eagleSpecies == "Golden Eagle":
    eagleType = "GOEA"
if eagleSpecies == "Bald Eagle":
    eagleType = "BAEA"

logfile = ""



ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
dirst = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d_%H%M%S')

scratchWS = summaryOutput[0:str(summaryOutput).rfind("\\")+1]  + "CEET_" + str(dirst) + "\\"
os.makedirs(scratchWS)

cwd = os.path.dirname(sys.argv[0])

focalProjectCopy = os.path.join(scratchWS, "focalProjectCopy.shp")
arcpy.CopyFeatures_management(focalProject, focalProjectCopy)



summaryHeader = ""
summaryHeader += "logfile start \n"

if eagleType == "BAEA":
    summaryHeader += "US FWS Eagle Mapping Tool -- Summary Results (Bald Eagle), run "+ str(st) + "\n\n"
elif eagleType == "GOEA":
    summaryHeader += "US FWS Eagle Mapping Tool -- Summary Results (Golden Eagle), run "+ str(st) + "\n\n"

logfile += "\n"
logfile += "TOOL INPUT PARAMETERS\n"

summaryHeader += "Focal Project Area "

prjCursorName = arcpy.UpdateCursor(focalProjectCopy)
prjRow = prjCursorName.next()
prjName = prjRow.getValue("Prj")
del prjCursorName,prjRow

summaryHeader += prjName + "\n"



cursor = arcpy.UpdateCursor(focalProjectCopy)
row = cursor.next()

takeVal = row.getValue(eagleType + "Take")
del row, cursor

summaryHeader += "Predicted eagle take, " + str(takeVal)  + "\n"

# validate date entry
match = re.match(r'(^[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$)',dateRange)

if match != None:
    date = str(dateRange).split("-")
    dt = datetime.datetime.now()
    if int(date[0]) < int(date[1]):
        if int(date[1]) > dt.year:
            date[1] = dt.year
            dateRange = date[0] + "-" + str(dt.year)
else:
    dt = datetime.datetime.now()
    dateRange = "1950-" + str(dt.year)


descriptor = arcpy.Describe(focalProjectCopy)
geometryType = descriptor.shapeType

if geometryType == 'Point':
    focalProjectFinal = os.path.join(scratchWS, "focalProjectFinal.shp")
    arcpy.Buffer_analysis(focalProjectCopy,focalProjectFinal, " .001 MILES")
else:
    focalProjectFinal = focalProjectCopy

maxProjectDistance = str(float(lapRadius) * 2)
focalProjectSearchPoly = os.path.join(scratchWS, "focalProjectSearchPoly.shp") 
arcpy.Buffer_analysis(focalProjectFinal, focalProjectSearchPoly, maxProjectDistance + " MILES")

prjList = []
nonEstablishedPrj = []
if nonEstablishedProjects != "#":

    fP = arcpy.GetParameterAsText(2)
    projects = fP.split(";")
    if projects.__len__() == 0:
        projects = fP

    for prj in projects:
        prjList.append(prj)
        nonEstablishedPrj.append(prj)

if establishedProjects != "#":
    prjList.append(establishedProjects)

singleProject = False
if len(prjList) == 0:
    singleProject = True
    prjList.append(focalProjectCopy)

pointList = []
polyList = []

for prj in prjList:
    descriptor = arcpy.Describe(prj)
    geometryType = descriptor.shapeType

    if geometryType == 'Polygon':
        polyList.append(str(prj))
    elif geometryType == 'Point':
        pointList.append(str(prj))

# buffer points slightly

pointMerge = os.path.join(scratchWS,"pointMerge.shp")   
pointBuffer = os.path.join(scratchWS,"pointBuffer.shp")

if pointList.__len__() == 1:
    try:
        arcpy.Buffer_analysis(pointList[0],pointBuffer,".001 MILES")
    except Exception as e:
        pass
elif pointList.__len__() > 1:
    arcpy.Merge_management(pointList,pointMerge)
    pointBuffer = os.path.join(scratchWS,"pointBuffer.shp")
    arcpy.Buffer_analysis(pointMerge,pointBuffer,".001 MILES")

poly = False
pt = False

if polyList.__len__() > 0:
    poly = True
if pointList.__len__() > 0:
    pt = True
    
projectShapefile = os.path.join(scratchWS,"projectShapefile.shp")

if poly == True and pt == True:
    if len(polyList) > 1:
        polyList2 = os.path.join(scratchWS,"polyList2.shp")            
        arcpy.Merge_management(polyList,polyList2)
        arcpy.Merge_management([pointBuffer,polyList2],projectShapefile)
    else:
        arcpy.Merge_management([pointBuffer,polyList],projectShapefile)
elif poly == True:
    arcpy.Merge_management(polyList,projectShapefile)
elif pt == True:
    arcpy.Merge_management(pointBuffer,projectShapefile)
    
count = 0
tempBufferIntersect = []

projectShapefileClipped = os.path.join(scratchWS,"projectShapefileClipped.shp") 
arcpy.Buffer_analysis(focalProjectFinal,focalProjectSearchPoly, maxProjectDistance + " MILES")
arcpy.Clip_analysis(projectShapefile,focalProjectSearchPoly,projectShapefileClipped) 
  
prjFIDs = spatialfunctions.retrieveFID(projectShapefileClipped)

otherProjectLog = []
if singleProject == True:
    singlecalcfunction.singlecalc(focalProjectFinal,"#",spitsLayer,focalProjectSearchPoly,unpermittedTakeLayer,
              managementLayer,lapRadius,lowerThreshold,upperThreshold,summaryOutput,count,dateRange,eagleType,cwd,scratchWS)
else:
    for ID in prjFIDs:
        otherProjectShapefile = os.path.join(scratchWS,"otherProject" + str(count) + ".shp")
        otherProjectLog.append(otherProjectShapefile)
        spatialfunctions.extractShapefileRow(projectShapefileClipped,otherProjectShapefile, "FID",str(ID),"NUMERIC")
        singlecalcfunction.singlecalc(focalProjectFinal,otherProjectShapefile,spitsLayer,focalProjectSearchPoly,unpermittedTakeLayer,
                  managementLayer,lapRadius,lowerThreshold,upperThreshold,summaryOutput,count,dateRange,eagleType,cwd,scratchWS)
        count += 1
        
spitsLog = []
if len(spitsLayer) > 1:
    
    tempBufferIntersect = []
  
    intersectedSpits = os.path.join(scratchWS,"intersectedSpits" + str(count) + ".shp")
    arcpy.Intersect_analysis([spitsLayer,focalProjectSearchPoly],intersectedSpits)    
    intersectedSpitsBuffer = os.path.join(scratchWS,"intersectedSpitsBuffer" + str(count) + ".shp")
    arcpy.Buffer_analysis(intersectedSpits,intersectedSpitsBuffer,".001 MILES")
    
    tempSpitsBuffer = os.path.join(scratchWS,"tempSpitsBuffer" + str(count) + ".shp")
    
    spitsLog.append(tempSpitsBuffer)
    
    spitsBufferShapefile = os.path.join(scratchWS,"spitsBufferShapefile" + str(count) + ".shp")
    
    spatialfunctions.extractShapefileRow(intersectedSpitsBuffer,tempSpitsBuffer," SPCode ",eagleType," STRING")
    
    tempSpitsBufferFinal = os.path.join(scratchWS,"tempSpitsBufferFinal" + str(count) + ".shp")
    
    spitsLog.append(tempSpitsBufferFinal)
    
    #spatialfunctions.extractShapefileRow(intersectedSpitsBuffer,tempSpitsBufferFinal," Auth ","1","NUMERIC")
    
    
    spitsFIDs = spatialfunctions.retrieveFID(intersectedSpitsBuffer) #(tempSpitsBufferFinal)
    
    for ID in spitsFIDs:
        
        spitsProjectPoints = os.path.join(scratchWS,"spitsProjectPoints" + str(count) + ".shp")
        spitsLog.append(spitsProjectPoints)
        spatialfunctions.extractShapefileRow(tempSpitsBuffer,spitsProjectPoints, "FID", str(ID),"NUMERIC")
        
        eagleName = eagleType + "Take"
        
        arcpy.CalculateField_management(spitsProjectPoints, eagleName, "!Auth!", "PYTHON_9.3", "")
        
        arcpy.CalculateField_management(spitsProjectPoints, "Prj", "!PermNo!", "PYTHON_9.3", "")     
        
        singlecalcfunction.singlecalc(focalProjectFinal,spitsProjectPoints,spitsLayer,focalProjectSearchPoly,unpermittedTakeLayer,managementLayer,lapRadius,
                  lowerThreshold,upperThreshold,summaryOutput,count,dateRange,eagleType,cwd,scratchWS)

        count += 1

so = open(summaryOutput)
soList = list(so)

soList.insert(0, summaryHeader)
so.close()

so = open(summaryOutput,"w")
for item in soList:
    so.write(str(item))
so.close()

logfile += "Focal project layer," + focalProject +  "\n"
logfile += "Non-established projects:\n"
if len(nonEstablishedPrj) == 0:
    logfile += "    #\n"
else:
    for item in nonEstablishedPrj:
        logfile += "   " + item + "\n"
logfile += "\n"
logfile += "Established projects," + establishedProjects + "\n"
logfile += "Permitted take," + spitsLayer + "\n"
logfile += "Unpermitted take," + unpermittedTakeLayer + "\n"
logfile += "Management Unit shapefile," + managementLayer +"\n"
logfile += "Natal dispersal distance (miles)," + lapRadius +"\n"
logfile += "Take benchmark % (lower)," + lowerThreshold + "\n"
logfile += "Take benchmark % (upper)," + upperThreshold + "\n"
logfile += "Date range," + dateRange + "\n"
logfile += "Summary output file," + summaryOutput + "\n"
logfile += "\n"
logfile += "OUTPUT FILES\n"
if focalProjectCopy == focalProjectFinal:
    logfile += focalProjectCopy + "\n"       
else:
    logfile += focalProjectCopy + "\n"       
    logfile += focalProjectFinal + "\n" 
logfile += focalProjectSearchPoly + "\n"   

if pointList.__len__() == 1:
    logfile += pointBuffer + "\n"   
elif pointList.__len__() > 1:
    logfile += pointMerge + "\n"   

if singleProject == False:
    for line in otherProjectLog:
        logfile +=str(line) + "\n"

if len(spitsLayer) > 1:
    
    logfile += intersectedSpitsBuffer + "\n"
    for line in spitsLog:
        logfile +=str(line) + "\n"
summaryfunctions.summaryAppend(summaryOutput,lowerThreshold,upperThreshold,lowerThreshold,scratchWS)

fh = open(summaryOutput,"a")
for item in logfile:
    fh.write(item)

fhll = open(summaryOutput[:-4] + "log.txt","r")
logList = list(fhll)
fhll.close()

for item in logList:
    fh.write(item)
fh.close()

os.remove(summaryOutput[:-4] + "log.txt")

try:
    os.remove(os.path.join(scratchWS,"unpermit.txt"))
except:
    pass

