import sys,os
import arcpy


def summaryAppend(summaryOutput,lowerThres,upperThreshold,lowerThreshold,scratchWS):

    fh = open(summaryOutput,"r")
    
    header = "Established Projects with Overlapping LAPs,Estimated Annual Take,Percent Overlap Focal Project,Overlapping Take Adjustment"
    
    lwrThresPct = lowerThreshold + "% LAP Benchmark"
    uprThresPct = upperThreshold + "% LAP Benchmark"
    num_eagles = "total"
    tmpSummary = list(fh)
    
    indexHeader = int( str([i for i, s in enumerate(tmpSummary) if header in s])[1:-1])
    
    try:
        indexUpperPct = int( str([i for i, s in enumerate(tmpSummary) if uprThresPct in s])[1:-1])
        indexLowerPct = int( str([i for i, s in enumerate(tmpSummary) if lwrThresPct in s])[1:-1])
    
        
        indexNumEagles = int( str([i for i, s in enumerate(tmpSummary) if num_eagles in s])[1:-1])
        lwrPctBench = float(str(tmpSummary[indexLowerPct])[str(tmpSummary[indexLowerPct]).rfind(",")+1:len(tmpSummary[indexLowerPct])])
        upperPctBench = float(str(tmpSummary[indexUpperPct])[str(tmpSummary[indexUpperPct]).rfind(",")+1:len(tmpSummary[indexUpperPct])])
    except:
        indexUpperPct = 0
        indexLowerPct = 0
        indexNumEagles = 0
        upperPctBench = 0
    
    overlapValues = []
    indexHeader += 1
    while(1):
        try:
            tmpLine = tmpSummary[indexHeader]
        except:
            break
        
        if len(tmpLine) <= 2:
            break
        else:
            overlapValues.append(tmpLine)
        indexHeader += 1
        
        if indexHeader > 1000:
            break
    fh.close()
    sumOverlap = 0
    totAnnualTake = 0
    
    for item in overlapValues:
        splitStr = str(item).split(",")
        spits = splitStr[0][0:5]
        if spits == "SPITS":
            pass
        else:
            try:
                sumOverlap += float(splitStr[3])
                totAnnualTake += float(splitStr[1])
            except:
                sumOverlap = 0
                totAnnualTake = 0
   
    
    adjLwrPctBench = lwrPctBench - sumOverlap
    adjUpperPctBench = upperPctBench - sumOverlap
    
    numEagles = str(tmpSummary[indexNumEagles]).split(",")
    
    totalEagles = float(numEagles[len(numEagles)-1])
    
    try:
        pctLapOfOverlapTake = sumOverlap/totalEagles * 100
    except:
        pctLapOfOverlapTake = 0
    
    fh = open(summaryOutput,"a")
    
    fh.write("All Projects (total),"+ str(totAnnualTake) + ",," + str(sumOverlap) + "\n")
    fh.write("\n")  
    fh.write("Percent LAP @ Current Levels,,%.2f"%(pctLapOfOverlapTake) + "\n")
    fh.write(str(numEagles[0])[:str(numEagles[0]).find(" ")] + " Adjusted Benchmarks,Adjusted LAP (" + str(lowerThres) + "%)," + str(adjLwrPctBench)+ "\n")
    fh.write(",Adjusted LAP (" + upperThreshold + "%)," + str(adjUpperPctBench)+ "\n")
    
   
    
    fh.write("\n")        
    try:
        up = open(os.path.join(scratchWS,"unpermit.txt"))
        unpmt = list(up)
    
        for line in unpmt:
            fh.write(str(line))
        up.close()
    except:
        pass
    
    fh.close()