#Copyright (c) 2014, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.

import os,arcpy

def buildOverlap(originalBuffer,centroid,reducedBuffer,finalBuffer,workspace):
    tempJoin = arcpy.CreateScratchName("temp","","Shapefile",workspace)
    arcpy.SpatialJoin_analysis(centroid,originalBuffer,tempJoin,"JOIN_ONE_TO_MANY")
    arcpy.SpatialJoin_analysis(reducedBuffer,tempJoin,finalBuffer,"JOIN_ONE_TO_MANY")
    arcpy.Delete_management(tempJoin)
   
def buildProjectCentroid(polyShapefile,prjPoint,workspace):
    
    xField = "xCentroid"
    yField = "yCentroid"
    
    fieldPrecision = 18
    fieldScale = 11
    
    polyShapefileFinal = os.path.join(workspace,polyShapefile[:-4] + "1.shp")
    
    arcpy.Copy_management(polyShapefile,polyShapefileFinal)

    arcpy.AddField_management(polyShapefileFinal, "centroidx", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
    arcpy.AddField_management(polyShapefileFinal, "centroidy", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
    
    arcpy.CalculateField_management(polyShapefileFinal, "centroidx", "float(!SHAPE.CENTROID!.split()[0])", "PYTHON", "")
    arcpy.CalculateField_management(polyShapefileFinal, "centroidy", "float(!SHAPE.CENTROID!.split()[1])", "PYTHON", "")
      
    rows = arcpy.SearchCursor(polyShapefileFinal)
    fields = arcpy.ListFields(polyShapefileFinal, "", "All")
    
    
    x=""
    y=""
    
    xydict = dict()
    
    if prjPoint != "#":
        pt = arcpy.Point()
        coorList=[]
        for row in rows:
            x = row.getValue("centroidx")
            y = row.getValue("centroidy")
            exists = False
            for key in xydict.iterkeys():
                if xydict[key] == x + y:
                    exists = True
                    
            if exists == False:
                strval = x + y
                xydict[row] = strval
                pt.X = x
                pt.Y = y
                coorList.append(arcpy.PointGeometry(pt))
        arcpy.CopyFeatures_management(coorList, prjPoint)
    return os.path.join(workspace,polyShapefile[:-4] + "1.shp")
        
def intersectPolyPoint(pointsLayer,polyLayer,intersectLayer):
    arcpy.Intersect_analysis([polyLayer,pointsLayer],intersectLayer)    


def intersectPoly(managementLayer,prjBufferLayer,intersectLayer):
    arcpy.Intersect_analysis([managementLayer,prjBufferLayer],intersectLayer)
    addAreaField(intersectLayer,"Miles_sq,MUtotArea")
    addAreaField(managementLayer,"MUtotArea")


def addAreaField(areaLayer,fieldsToAdd):
    getFields = fieldsToAdd.split(",")
    outputField = areaLayer[:-4] + ".dbf"
    exp = "!SHAPE.area@squaremiles!"
    for field in getFields:
        arcpy.AddField_management(outputField, field, "DOUBLE")
        arcpy.CalculateField_management(outputField, field, exp, "PYTHON")


def addValueField(areaLayer,fieldToAdd,sourceField):
    getFields = fieldToAdd.split(",")
    outputField = areaLayer[:-4] + ".dbf"
    exp = "!" + sourceField + "!"
    arcpy.AddField_management(areaLayer, fieldToAdd, "INTEGER")
    arcpy.CalculateField_management(outputField, fieldToAdd, exp, "PYTHON")
    

def extractManagementAreas(muLayer,muName,muArea,intersectLayer,intersectName,intersectArea):
    muCursor = arcpy.UpdateCursor(muLayer)
    intersectCursor = arcpy.UpdateCursor(intersectLayer)
    
    muRow = muCursor.next()
    intersectRow = intersectCursor.next()
    tempRow = dict()
    
    print "compute management areas..."
    
    # find all management areas that intersect with project
    while intersectRow:
        while muRow:
            muAreaName = muRow.getValue(muName)
            intersectAreaName = intersectRow.getValue(intersectName)
            areaVal = muRow.getValue(muArea)
            doNotAppend = False
            if intersectAreaName == muAreaName:
                if not tempRow.has_key(intersectAreaName):
                    tempRow[muAreaName] = [areaVal]
                else:
                    for key in tempRow.iterkeys():
                        x = tempRow[key]
                        for val in x:
                            if val == areaVal:
                                doNotAppend = True
                    if doNotAppend == False:       
                        tempRow[muAreaName].append(areaVal)
                    else:
                        doNotAppend = False 
                    
            muRow = muCursor.next()
        
        intersectRow = intersectCursor.next()    
        del muRow,muCursor
        muCursor = arcpy.UpdateCursor(muLayer)
        muRow = muCursor.next()
    
    intersectCursor = arcpy.UpdateCursor(intersectLayer)
    intersectRow = intersectCursor.next()
    
    # sum all of the management unit pieces with a particular ID
    # to get a total area for the management units that intersect project buffers
            
    for key in tempRow.iterkeys():
        while intersectRow:
            if intersectRow.getValue(intersectName) == key:
                x = sum(tempRow[key])
                intersectRow.setValue(intersectArea,x) 
                intersectCursor.updateRow(intersectRow)
            intersectRow = intersectCursor.next()
        del intersectRow,intersectCursor
        intersectCursor = arcpy.UpdateCursor(intersectLayer)
        intersectRow = intersectCursor.next()


def extractShapefileRow(shapefile,outputShapefile,fieldName,value,fieldType):
    
    if fieldType == "NUMERIC":
        expression = fieldName + " = " + value 
    else:
        expression = fieldName + " = '" + value + "'"
    arcpy.Select_analysis(shapefile, outputShapefile, expression)
    
  
def retrieveFID(layer):
    prjCursor = arcpy.UpdateCursor(layer)
    prjRow = prjCursor.next()
    prjFIDs = []
    while prjRow:
        PrjFID = prjRow.getValue("FID")
        prjFIDs.append(PrjFID)
        prjRow = prjCursor.next()
    
    del prjCursor,prjRow
    return prjFIDs
  
    
