#Copyright (c) 2016, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.


import os, sys
import subprocess
import archook
try:
    import archook #The module which locates arcgis
    archook.get_arcpy()
    import arcpy
except ImportError:
    pass

def returnToolboxPathName(layer, index):
    layer = arcpy.GetParameterAsText(index)
    desc = arcpy.Describe(layer)
    return os.path.join(desc.path, desc.name)

sysArgs = sys.argv[1:]

eagleSpecies = sysArgs[0]
focalProject = sysArgs[1]
nonEstablishedProjects = sysArgs[2]
establishedProjects = sysArgs[3]
spitsLayer = sysArgs[4]
unpermittedTakeLayer = sysArgs[5]
managementLayer = sysArgs[6]
lapRadius = sysArgs[7]
lowerThreshold = sysArgs[8]
upperThreshold = sysArgs[9]
dateRange = sysArgs[10]
summaryOutput = sysArgs[11]

if len(focalProject) > 1:
    focalProject = returnToolboxPathName(focalProject, 1)
if len(establishedProjects) > 1:
    establishedProjects = returnToolboxPathName(establishedProjects, 3)
if len(spitsLayer) > 1:
    spitsLayer = returnToolboxPathName(spitsLayer, 4)
if len(unpermittedTakeLayer) > 1:
    unpermittedTakeLayer = returnToolboxPathName(unpermittedTakeLayer, 5)
if len(managementLayer) > 1:
    managementLayer = returnToolboxPathName(managementLayer, 6)

currentDir = str(sys.argv[0])[0:str(sys.argv[0]).rfind("\\")]

batchFile = os.path.join(currentDir,"cet.bat")

batchScript = "python.exe " + currentDir + "\\core.py" + " \"" + eagleSpecies + "\" " + focalProject + " " + nonEstablishedProjects + " " \
              + establishedProjects + " " + spitsLayer + " " + unpermittedTakeLayer + " " + managementLayer \
              + " " + lapRadius + " " + lowerThreshold + " " + upperThreshold + " " + dateRange + " " + summaryOutput

fh = open(batchFile,"w")
fh.write(batchScript)
fh.close()

p = subprocess.Popen('cet.bat',cwd=currentDir,creationflags=subprocess.CREATE_NEW_CONSOLE)
stdout, stderr = p.communicate()
p.wait()

