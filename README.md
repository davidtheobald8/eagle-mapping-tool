Spatial Analysis Tool for Cumulative Effects of Eagle Take
============================================

This repository contains the ArcGIS toolbox, Python code, and tool overview document associated with the cumulative effects of eagle take tool. Files included are:

File                                |Description
--------------------------------|--------------------------------------------------------------------------
*.py                               | Python ArcGIS scripts
*.tbx                           | ArcGIS tool box 
*.pdf                            | Technical description of the tool and how to use it.

### Contact information ###

Please contact David Theobald (<davet@csp-inc.org>) with any questions or concerns about the repository.