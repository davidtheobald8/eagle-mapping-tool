#Copyright (c) 2014, Conservation Science Partners, Inc.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
   #list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
   #this list of conditions and the following disclaimer in the documentation
   #and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.

import spatialfunctions
import takefunctions
import buildOverlapDistribution
import os,arcpy

def singlecalc(focalProject,baseLayer,spitsShapefile,focalProjectSpitsBuffer,unpermittedTake,manageShapefile,lapRadius,
              lowerThreshold,upperThreshold,summaryOutput,count,dateVal,eagleType,cwd,scratchWS):
    logfile = ""
    finalPrjList = []
    finalPrjList.append(focalProject)
    if baseLayer != "#":
        finalPrjList.append(baseLayer)
    projectShapefile = os.path.join(scratchWS,"projectShapefile" + str(count) + ".shp")
    logfile += projectShapefile + "\n"
    arcpy.Merge_management(finalPrjList,projectShapefile)

    prjbuffer = os.path.join(scratchWS,"prjbuffer" + str(count) + ".shp") 
    arcpy.Buffer_analysis(projectShapefile,prjbuffer, lapRadius + " MILES")
    logfile += prjbuffer + "\n"
    prjCursor = arcpy.UpdateCursor(projectShapefile)
    prjRow = prjCursor.next()
    prjIDs = set()
    eagleTake = eagleType + "Take"
    focalPrjValue = prjRow.getValue(eagleTake)
    focalPrjFID = prjRow.getValue("FID")
   
    
    del prjCursor,prjRow
    
    prjbufferpen = os.path.join(scratchWS,"prjbufferpen" + str(count) + ".shp")
    
    logfile += scratchWS + r"prjbufferpen" + str(count) + ".shp" + "\n"
    
    arcpy.Union_analysis(prjbuffer,prjbufferpen)
    
    prjpointspen = os.path.join(scratchWS,"prjpointspen" + str(count) + ".shp") 
    
    logfile += scratchWS + r"prjpointspen" + str(count) + ".shp" + "\n"
    
    localName = spatialfunctions.buildProjectCentroid(prjbufferpen,prjpointspen,scratchWS)
    
    logfile += localName + "\n"
    
    prjBufferFinal = os.path.join(scratchWS,"prjbufferfinal" + str(count) + ".shp")
    
    logfile += scratchWS + r"prjbufferfinal" + str(count) + ".shp" + "\n"
    
    spatialfunctions.buildOverlap(prjbuffer,prjpointspen,prjbufferpen,prjBufferFinal,scratchWS) 
    
    manageBufferIntersect = os.path.join(scratchWS,"managebuffint" + str(count) + ".shp") #arcpy.CreateScratchName("bcrbuffint","","Shapefile",workspace=arcpy.env.)
    
    spatialfunctions.intersectPoly(manageShapefile,prjBufferFinal,manageBufferIntersect)
    
    logfile += scratchWS + r"managebuffint" + str(count) + ".shp" + "\n"
  
    intersected_take = ""
    spitsTakePoints = ""
    externalToProjectTake = ""
    
    
    
    
    if count == 0:
        if unpermittedTake != "#":
               
            unpermitted_take = os.path.join(scratchWS,"unpermittedtake" + str(count) + ".shp")
            logfile += unpermitted_take + "\n"
                        
            spatialfunctions.intersectPolyPoint(unpermittedTake,focalProjectSpitsBuffer,unpermitted_take)
            
            takefunctions.computePointTake("UNPMT",unpermitted_take,eagleType,dateVal,os.path.join(scratchWS,"unpermit.txt"))
           
    if (len(spitsShapefile) > 1):
            
            spitsTakePoints = os.path.join(scratchWS,"spitstakepoints" + str(count) + ".shp")             
            arcpy.CopyFeatures_management(spitsShapefile, spitsTakePoints)
            logfile += spitsTakePoints + "\n"
    
            intersected_take = os.path.join(scratchWS,"intersectedtake" + str(count) + ".shp")
            logfile += intersected_take + "\n"
            spatialfunctions.intersectPolyPoint(spitsTakePoints,manageBufferIntersect,intersected_take)
            
            #externalToProjectTake = os.path.join(scratchWS,"munitintersecttake" + str(count) + ".shp")
            #logfile += scratchWS + r"munitintersecttake" + str(count) + ".shp" + "\n"            
            #spatialfunctions.intersectPolyPoint(spitsTakePoints,manageShapefile,externalToProjectTake)
            #takefunctions.deleteFeatures(intersected_take,externalToProjectTake)
    
    if eagleType == "BAEA":
        spatialfunctions.extractManagementAreas(manageShapefile,"EMUName","MUtotArea",manageBufferIntersect,"EMUName","MUtotArea")        
        if baseLayer == "#":        
            buildOverlapDistribution.buildSingleProjectDistribution(manageBufferIntersect,focalPrjFID,"TARGET_FID","JOIN_FID","Miles_sq","EMUName",
                              manageShapefile,"MUtotArea","EaglePop",prjBufferFinal,"#","Lat","Long",spitsTakePoints,"Auth",
                              "#",eagleType,lowerThreshold,upperThreshold,summaryOutput)
        else:
         
            buildOverlapDistribution.buildOverlapDistribution(manageBufferIntersect,focalPrjFID,"TARGET_FID","JOIN_FID","Miles_sq","EMUName",
                          manageShapefile,"MUtotArea","EaglePop",prjBufferFinal,intersected_take,"Lat","Long",spitsTakePoints,"AUTH",
                          "#",eagleType,lowerThreshold,upperThreshold,summaryOutput)
    elif eagleType == "GOEA":
        spatialfunctions.extractManagementAreas(manageShapefile,"BCR_Name","MUtotArea",manageBufferIntersect,"BCR_Name","MUtotArea")        
        if baseLayer == "#":
            buildOverlapDistribution.buildSingleProjectDistribution(manageBufferIntersect,focalPrjFID,"TARGET_FID","JOIN_FID","Miles_sq","BCR_Name",
                                  manageShapefile,"MUtotArea","EaglePop",prjBufferFinal,"#","Lat","Long",spitsTakePoints,"Auth",
                                  "#", eagleType, lowerThreshold, upperThreshold, summaryOutput)            
        else:
            buildOverlapDistribution.buildOverlapDistribution(manageBufferIntersect,focalPrjFID,"TARGET_FID","JOIN_FID","Miles_sq","BCR_Name",
                      manageShapefile,"MUtotArea","EaglePop",prjBufferFinal,intersected_take,"Lat","Long",spitsTakePoints,"Auth",
                      "#", eagleType, lowerThreshold, upperThreshold, summaryOutput)
    
    
    fh = open(summaryOutput[:-4] + "log.txt","a")
    fh.write(logfile)
    fh.close()
    
